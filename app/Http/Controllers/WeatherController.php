<?php namespace App\Http\Controllers;


use App\Services\Http\Curl\CurlHttpClient;
use App\Services\Weather\Forecast;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class WeatherController extends Controller
{

    public function index()
    {
        if (!Cache::has('weather')) {
            $curl = new CurlHttpClient();
            $url = env('API_URL') . '&lat=' . env('API_LAT') . '&lon=' . env('API_LONG') . '&appid=' . env('API_KEY');
            $forecast = $curl->get($url);
            Cache::put('weather', $forecast, 3600);
            Cache::put('weather_update', date('U'), 3600);
        }

        $forecastParse = Cache::get('weather');
        $forecastDate = (new DateTime())->setTimestamp(Cache::get('weather_update'));

        $forecast = new Forecast($forecastParse);

        return view('weather.forecast.forecast', ['forecast' => $forecast, 'update' => $forecastDate->format('d/m/Y H:i:s')]);
    }

}
