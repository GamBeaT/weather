<?php namespace App\Services\Weather;

use DateTime;

class CurrentDay implements Day
{

    protected $date;
    protected $temp;
    protected $feelsLike;
    protected $sunrise;
    protected $sunset;

    public function __construct(DateTime $date, float $temp, float $feelsLike, DateTime $sunrise, DateTime $sunset)
    {
        $this->date = $date;
        $this->temp = $temp;
        $this->feelsLike = $feelsLike;
        $this->sunrise = $sunrise;
        $this->sunset = $sunset;
    }

    public function getDate()
    {
        return $this->date->format('d/m/Y H:i:s');
    }

    public function getTemp()
    {
        return $this->temp;
    }

    public function getTempFeelsLike()
    {
        return $this->feelsLike;
    }

    public function isCold()
    {
        if ($this->temp < 20) {
            return true;
        }
        return false;
    }

    public function getSunrise()
    {
        return $this->sunrise->format('H:i');
    }

    public function getSunset()
    {
        return $this->sunset->format('H:i');
    }

}
