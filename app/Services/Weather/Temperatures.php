<?php namespace App\Services\Weather;

class Temperatures
{

    protected $temps = [];

    public function __construct(float $day, float $morn, float $eve, float $night)
    {
        $this->temps['day'] = $day;
        $this->temps['morn'] = $morn;
        $this->temps['eve'] = $eve;
        $this->temps['night'] = $night;
    }

    public function getTemp()
    {
        return $this->temps['day'];
    }

    public function getTempMorn()
    {
        return $this->temps['morn'];
    }

    public function getTempEve()
    {
        return $this->temps['eve'];
    }

    public function getTempNight()
    {
        return $this->temps['night'];
    }
}
