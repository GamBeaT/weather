<?php namespace App\Services\Weather;

interface DayForecast
{

    public function getTempMorn();
    public function getTempEve();
    public function getTempNight();
    public function getTempMornFeelsLike();
    public function getTempEveFeelsLike();
    public function getTempNightFeelsLike();

}
