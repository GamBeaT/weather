<?php namespace App\Services\Weather;

use DateTime;

class Forecast
{
    protected $forecast;
    protected $current;
    protected $days = [];

    public function __construct(string $weatherParse)
    {
        $this->forecast = json_decode($weatherParse);
        if (!$this->forecast) {
            throw new \InvalidArgumentException('Invalid Json string');
        }
        $this->parseCurrent();
        $this->parseDaily();
    }

    protected function parseCurrent()
    {
        $current = $this->forecast->current;
        $this->current = new CurrentDay(
            (new DateTime())->setTimestamp($current->dt),
          round($current->temp,1),
          round($current->feels_like,1),
            (new DateTime())->setTimestamp($current->sunrise),
            (new DateTime())->setTimestamp($current->sunset)
        );
    }

    protected function parseDaily()
    {
        $daily = $this->forecast->daily;
        foreach ($daily as $forecast) {
            $temps = new Temperatures(
                round($forecast->temp->day,1),
                round($forecast->temp->morn,1),
                round($forecast->temp->eve,1),
                round($forecast->temp->night,1)
            );
            $feelsLike = new Temperatures(
                round($forecast->feels_like->day,1),
                round($forecast->feels_like->morn,1),
                round($forecast->feels_like->eve,1),
                round($forecast->feels_like->night,1)
            );
            $this->days[] = new NextDay(
                (new DateTime())->setTimestamp($forecast->dt),
                $temps,
                $feelsLike,
                (new DateTime())->setTimestamp($forecast->sunrise),
                (new DateTime())->setTimestamp($forecast->sunset)
            );
        }
    }

    public function getCurrent()
    {
        return $this->current;
    }

    public function getForecast()
    {
        return $this->days;
    }

}
