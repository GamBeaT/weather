<?php namespace App\Services\Weather;

use DateTime;

class NextDay implements Day, DayForecast
{

    protected $date;
    protected $temp;
    protected $feelsLike;
    protected $sunrise;
    protected $sunset;

    public function __construct(DateTime $date, Temperatures $temp, Temperatures $feelsLike, DateTime $sunrise, DateTime $sunset)
    {
        $this->date = $date;
        $this->temp = $temp;
        $this->feelsLike = $feelsLike;
        $this->sunrise = $sunrise;
        $this->sunset = $sunset;
    }

    public function getDate()
    {
        return $this->date->format('d/m');
    }

    public function getTemp()
    {
        return $this->temp->getTemp();
    }

    public function getTempFeelsLike()
    {
        return $this->feelsLike->getTemp();
    }

    public function isCold()
    {
        if ($this->temp->getTemp() < 20) {
            return true;
        }
        return false;
    }

    public function getSunrise()
    {
        return $this->sunrise->format('H:i');
    }

    public function getSunset()
    {
        return $this->sunset->format('H:i');
    }

    public function getTempMorn()
    {
        return $this->temp->getTempMorn();
    }

    public function getTempEve()
    {
        return $this->temp->getTempEve();
    }

    public function getTempNight()
    {
        return $this->temp->getTempNight();
    }

    public function getTempMornFeelsLike()
    {
        return $this->feelsLike->getTempMorn();
    }

    public function getTempEveFeelsLike()
    {
        return $this->feelsLike->getTempEve();
    }

    public function getTempNightFeelsLike()
    {
        return $this->feelsLike->getTempNight();
    }
}
