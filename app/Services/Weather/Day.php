<?php namespace App\Services\Weather;

use DateTime;

interface Day
{

    public function getDate();
    public function getTemp();
    public function getTempFeelsLike();
    public function isCold();
    public function getSunrise();
    public function getSunset();

}
