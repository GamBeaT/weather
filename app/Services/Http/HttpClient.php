<?php namespace App\Services\Http;

interface HttpClient
{

    public function get(string $url): string;

}
