<?php namespace App\Services\Http\Curl;

use App\Services\Http\HttpClient;

class CurlHttpClient implements HttpClient
{

    public function get(string $url): string
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        return $output;
    }

}
