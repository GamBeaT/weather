<div class="card">
    <h5 class="card-header">Pogoda na teraz - {{ $forecast->getCurrent()->getDate() }}</h5>
    <div class="card-body">
        <h5 class="card-title">Temperatura: {{ $forecast->getCurrent()->getTemp() }} &ordm;C
            @if ($forecast->getCurrent()->isCold())
                <i class="fas fa-temperature-low text-primary"></i>
            @else
                <i class="fas fa-temperature-high text-danger"></i>
            @endif
        </h5>
        <p class="card-text">Odczuwalna: {{ $forecast->getCurrent()->getTempFeelsLike() }} &ordm;C</p>
        <p class="card-text"><i class="far fa-sun text-warning"></i> Wschód o: {{ $forecast->getCurrent()->getSunrise() }}</p>
        <p class="card-text"><i class="far fa-moon text-info"></i> Zachód o: {{ $forecast->getCurrent()->getSunset() }}</p>
    </div>
</div>
