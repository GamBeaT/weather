@extends('weather.index')

@section('content')
    <div class="container col-md-5 justify-content-center">
        <div class="card">
            <div class="card-header">
                Pogoda dla Warszawy
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                    <span style="font-size: 24px">
                        {{ $forecast->getCurrent()->getTemp() }}&ordm;C
                        @if ($forecast->getCurrent()->isCold())
                            <i class="fas fa-temperature-low text-primary"></i>
                        @else
                            <i class="fas fa-temperature-high text-danger"></i>
                        @endif
                    </span>
                    <br />
                    <span class="text-muted">
                        Odczuwalna: {{ $forecast->getCurrent()->getTempFeelsLike() }} &ordm;C
                    </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <i class="far fa-sun text-warning"></i> Wschód o: {{ $forecast->getCurrent()->getSunrise() }}
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="far fa-moon text-info"></i> Zachód o: {{ $forecast->getCurrent()->getSunset() }}
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tr>
                                @foreach ($forecast->getForecast() as $key => $day)
                                <td>
                                    <b>{{ $day->getDate() }}<br /></b>
                                    {{ $day->getTemp() }}&ordm; <span class="text-muted">{{ $day->getTempNight() }}&ordm;</span>
                                </td>
                                @endforeach
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted">
                Ostatnia aktualizacja: {{ $update }}
            </div>
        </div>

    </div>
@endsection
