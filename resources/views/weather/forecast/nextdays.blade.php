<br />
<div class="accordion" id="forecast">
@foreach ($forecast->getForecast() as $key => $day)
    <div class="card">
        <div class="card-header" id="day{{ $key }}">
            <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#dayForecast{{ $key }}" aria-expanded="true" aria-controls="collapseOne">
                    Pogoda na {{ $day->getDate() }}
                </button>
            </h2>
        </div>

        <div id="dayForecast{{ $key }}" class="collapse" aria-labelledby="day{{ $key }}" data-parent="#forecast">
            <div class="card-body">
                <h5 class="card-title">Temperatura: {{ $day->getTemp() }} &ordm;C
                    @if ($day->isCold())
                        <i class="fas fa-temperature-low text-primary"></i>
                    @else
                        <i class="fas fa-temperature-high text-danger"></i>
                    @endif
                </h5>
                <p class="card-text">Odczuwalna: {{ $day->getTempFeelsLike() }} &ordm;C</p>
                <p class="card-text"><i class="far fa-sun text-warning"></i> Wschód o: {{ $day->getSunrise() }}</p>
                <p class="card-text"><i class="far fa-moon text-info"></i> Zachód o: {{ $day  ->getSunset() }}</p>
            </div>
        </div>
    </div>
@endforeach
</div>
